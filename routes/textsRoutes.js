import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config';
import { texts } from '../data';

const router = Router();

router.get('/:id', (req, res) => {
    const { id } = req.params;
    return res.status(200).json({
        result: texts[id],
    });
});
export default router;
