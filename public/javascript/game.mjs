import {
    getRoomBlock,
    showMessage,
    renderPlayerBar,
    createTimerBlock,
} from './DOMRender.mjs';
import { addClass, removeClass, createElement } from './helper.mjs';
const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

export const socket = io('', { query: { username } });

const container = document.querySelector('.availible-rooms');
const createBtn = document.querySelector('#add-room-btn');
const leaveRoomBtn = document.querySelector('#quit-room-btn');
const readyBtn = document.querySelector('#readyBtn');

window.addEventListener('load', () => {
    socket.emit('CHECK_USER', sessionStorage.getItem('username'));
});

readyBtn.addEventListener('click', () => {
    const roomId = document.querySelector('.room-name').innerText;
    socket.emit('CHANGE_STATUS', {
        room: roomId,
        name: sessionStorage.getItem('username'),
    });
});
createBtn.addEventListener('click', () => {
    const roomName = prompt('Enter room name: ');
    if (roomName) {
        socket.emit('CREATE_ROOM', { name: roomName });
    }
});
leaveRoomBtn.addEventListener('click', () => {
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('LEAVE_ROOM', roomName);
});

const createRoom = (data) => {
    const roomLi = getRoomBlock(data);
    const roomJoinBtn = roomLi.querySelector('.join-btn');
    roomJoinBtn.addEventListener('click', () => {
        socket.emit('JOIN_ROOM', data.name);
    });
    container.append(roomLi);
};

const updateRooms = (rooms) => {
    container.innerHTML = '';
    if (rooms.length > 0) {
        rooms.forEach((item) => {
            const data = { name: item[0], users: item[1] };
            if (data.users > 0) {
                const roomLi = getRoomBlock(data);
                const roomJoinBtn = roomLi.querySelector('.join-btn');
                roomJoinBtn.addEventListener('click', () => {
                    socket.emit('JOIN_ROOM', data.name);
                });
                if (data.users != 5) container.append(roomLi);
            } else {
                socket.emit('DELETE_ROOM', data.name);
            }
        });
    }
};

const joinRoom = (data) => {
    addClass(document.querySelector('#rooms-page'), 'display-none');
    removeClass(document.querySelector('#game-page'), 'display-none');
    const ul = document.querySelector('.user-list-ul');
    ul.innerHTML = '';
    const statuses = new Map(data.statusesArr);
    for (let i = 0; i < data.usersInRoom.length; i++) {
        const currentUser = data.usersInRoom[i];
        renderPlayerBar(
            currentUser,
            i + 1,
            statuses.get(currentUser),
            data.roomId,
        );
    }
};

const leaveRoomUser = () => {
    removeClass(document.querySelector('#rooms-page'), 'display-none');
    addClass(document.querySelector('#game-page'), 'display-none');
};

const leaveRoom = (data) => {
    const ul = document.querySelector('.user-list-ul');
    ul.innerHTML = '';
    const statuses = new Map(data.statusesArr);
    for (let i = 0; i < data.usersInRoom.length; i++) {
        const currentUser = data.usersInRoom[i];
        renderPlayerBar(
            currentUser,
            i + 1,
            statuses.get(currentUser),
            data.roomId,
        );
    }
    if (data.usersInRoom.length == 0) {
        socket.emit('DELETE_ROOM', data.roomId);
    }
};

const updateRoom = (data) => {
    const ul = document.querySelector('.user-list-ul');
    console.log(data);
    ul.innerHTML = '';
    for (let i = 0; i < data.roomUsers.length; i++) {
        const currentuser = data.roomUsers[i];
        let userStatus = data.statusesArr.filter(
            (item) => item[0] == currentuser,
        );
        renderPlayerBar(currentuser, i + 1, userStatus[0][1]);
    }
};

const updateRoomUser = (data) => {
    const currentuser = sessionStorage.getItem('username');
    let userStatus = data.statusesArr.filter((item) => item[0] == currentuser);
    if (userStatus[0][1] == 1) {
        readyBtn.innerText = 'Not ready';
    } else {
        readyBtn.innerText = 'Ready';
    }
};

const startTimer = (seconds) => {
    createTimerBlock(seconds);
    document.querySelector('#quit-room-btn').style.display = 'none';
    const roomName = document.querySelector('.room-name').innerText;

    socket.emit('CREATE_TEXT', roomName);
    const timerP = document.querySelector('.timer');
    timerP.innerText = seconds;
    const timer = setInterval(() => {
        timerP.innerText = --seconds;
        if (seconds == 0) {
            clearInterval(timer);
            startGame();
        }
    }, 1000);
};
const startGame = () => {
    document.querySelector('.timer').remove();
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('SPAWN_TEXT', roomName);
    window.addEventListener('keyup', keyHandler);
    socket.emit('SPAWN_GAME_TIMER', roomName);
};

async function getTextById(id) {
    const roomName = document.querySelector('.room-name').innerText;
    const resultText = await fetch('http://localhost:3002/game/texts/' + id);
    const resultJson = await resultText.json();
    const text = resultJson.result;
    socket.emit('SET_ROOM_TEXT', { text, roomName });
}
const spawnText = (text) => {
    const area = document.querySelector('.game-area');
    let checker = document.querySelector('.game-text');
    if (!checker) {
        const p = createElement({
            tagName: 'p',
            className: 'game-text',
            attributes: {
                id: 'text-container',
            },
        });
        p.innerHTML = `<span class="typed"></span><span class="next">${
            text[0]
        }</span><span class='text-left'>${text.substr(1)}</span>`;
        area.append(p);
    }
};

const keyHandler = (e) => {
    const pressed = e.key;
    const nextChar = document.querySelector('.next').innerText;
    const typed = document.querySelector('.typed');
    const next = document.querySelector('.next');
    const left = document.querySelector('.text-left');
    if (pressed.codePointAt() >= 32 && pressed.codePointAt() <= 126) {
        if (pressed == nextChar) {
            typed.textContent += nextChar;
            next.textContent = left.textContent[0];
            left.textContent = left.textContent.substr(1);
        }
    }
    const roomName = document.querySelector('.room-name').innerText;
    let fulllength = typed.textContent.length + left.textContent.length;
    if (left.textContent.length > 0 || next.textContent.length == 1)
        fulllength++;
    socket.emit('PROGRESS_ADD', {
        username: sessionStorage.getItem('username'),
        roomId: roomName,
        length: fulllength,
        typed: typed.innerText.length,
    });
};

const endGame = () => {
    window.removeEventListener('keyup', keyHandler);
    const bars = document.getElementsByClassName('user-progress');
    const undoneUsers = [];
    for (let i of bars) {
        if (i.style.width != '100%') {
            undoneUsers.push({
                name: i.classList[1],
                width: i.style.width ? i.style.width : '0%',
            });
        }
    }
    undoneUsers.sort(function (a, b) {
        if (parseInt(a.width) > parseInt(b.width)) {
            return -1;
        } else if (parseInt(a.width) == parseInt(b.width)) {
            return 0;
        } else {
            return 1;
        }
    });
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('SET_STATUSES_UNREADY', roomName);
    socket.emit('GET_RESULT_TABLE', { undoneUsers, roomName });
};

socket.on('CREATE_ROOM', createRoom);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('CREATE_ROOM_ERROR', showMessage);
socket.on('JOIN_ROOM', joinRoom);
socket.on('LEAVE_ROOM_USER', leaveRoomUser);
socket.on('LEAVE_ROOM', leaveRoom);
socket.on('UPDATE_ROOM', updateRoom);
socket.on('UPDATE_ROOM_USER', updateRoomUser);
socket.on('START_TIMER', startTimer);
socket.on('CREATE_TEXT', getTextById);
socket.on('SPAWN_TEXT', spawnText);
socket.on('END_GAME', endGame);
socket.on('PROGRESS_ADD', (data) => {
    const progressBar = document.querySelector(`.user-progress.${data.name}`);
    if (progressBar) {
        progressBar.style.width = data.width + '%';
        if (data.width == 100) {
            addClass(progressBar, 'finished');
        }
    }
});
socket.on('SPAWN_GAME_TIMER', (seconds) => {
    const area = document.querySelector('.rightside');
    const checker = document.querySelector('.game-timer');
    if (!checker) {
        const timerblock = createElement({
            tagName: 'div',
            className: 'game-timer',
        });
        timerblock.innerHTML = `<span class='seconds-left'>${seconds}</span> second left`;
        area.append(timerblock);
        const timerSpan = document.querySelector('.seconds-left');
        timerSpan.innerText = seconds;
        const timer = setInterval(() => {
            timerSpan.innerText = --seconds;
            if (seconds == 0) {
                clearInterval(timer);
                endGame();
            }
        }, 1000);
    }
});

socket.on('GET_RESULT_TABLE', (resultTable) => {
    let resultstr = ``;
    let k = 1;
    for (let i of resultTable) {
        resultstr += `${k}) <span id="place-${k++}">${i}</span><br>`;
    }
    const checker = document.querySelector('.modal');
    if (!checker) {
        showMessage(resultstr);
        const roomName = document.querySelector('.room-name').innerText;

        document
            .querySelector('.modal-closebtn')
            .addEventListener('click', () => {
                socket.emit('RELOAD_ROOM_USER', roomName);
                document.querySelector('#quit-room-btn').style.display =
                    'block';
                document.querySelector('#readyBtn').style.display = 'block';
                document.querySelector('.modal')?.remove();
                document.querySelector('.game-text')?.remove();
                document.querySelector('.game-timer')?.remove();
            });
        document
            .querySelector('.modal-closebtn')
            .setAttribute('id', 'quit-results-btn');
    }
});

socket.on('RELOAD_ROOM', () => {
    document.querySelector('#quit-room-btn').style.display = 'block';
    document.querySelector('#readyBtn').style.display = 'block';
    document.querySelector('.modal')?.remove();
    document.querySelector('.game-text')?.remove();
});
